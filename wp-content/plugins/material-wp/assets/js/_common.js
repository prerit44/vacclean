!(function (R, W, e) {
    var L = R(document),
        F = R(W),
        V = R(document.body);
    (W.adminMenu = { init: function () {}, fold: function () {}, restoreMenuState: function () {}, toggle: function () {}, favorites: function () {} }),
        (W.columns = {
            init: function () {
                var n = this;
                R(".hide-column-tog", "#adv-settings").click(function () {
                    var e = R(this),
                        t = e.val();
                    e.prop("checked") ? n.checked(t) : n.unchecked(t), columns.saveManageColumnsState();
                });
            },
            saveManageColumnsState: function () {
                var e = this.hidden();
                R.post(ajaxurl, { action: "hidden-columns", hidden: e, screenoptionnonce: R("#screenoptionnonce").val(), page: pagenow });
            },
            checked: function (e) {
                R(".column-" + e).removeClass("hidden"), this.colSpanChange(1);
            },
            unchecked: function (e) {
                R(".column-" + e).addClass("hidden"), this.colSpanChange(-1);
            },
            hidden: function () {
                return R(".manage-column[id]")
                    .filter(":hidden")
                    .map(function () {
                        return this.id;
                    })
                    .get()
                    .join(",");
            },
            useCheckboxesForHidden: function () {
                this.hidden = function () {
                    return R(".hide-column-tog")
                        .not(":checked")
                        .map(function () {
                            var e = this.id;
                            return e.substring(e, e.length - 5);
                        })
                        .get()
                        .join(",");
                };
            },
            colSpanChange: function (e) {
                var t,
                    n = R("table").find(".colspanchange");
                n.length && ((t = parseInt(n.attr("colspan"), 10) + e), n.attr("colspan", t.toString()));
            },
        }),
        L.ready(function () {
            columns.init();
        }),
        (W.validateForm = function (e) {
            return !R(e)
                .find(".form-required")
                .filter(function () {
                    return "" === R(":input:visible", this).val();
                })
                .addClass("form-invalid")
                .find(":input:visible")
                .change(function () {
                    R(this).closest(".form-invalid").removeClass("form-invalid");
                }).length;
        }),
        (W.showNotice = {
            warn: function () {
                var e = commonL10n.warnDelete || "";
                return !!confirm(e);
            },
            note: function (e) {
                alert(e);
            },
        }),
        (W.screenMeta = {
            element: null,
            toggles: null,
            page: null,
            init: function () {
                (this.element = R("#screen-meta")), (this.toggles = R("#screen-meta-links").find(".show-settings")), (this.page = R("#wpcontent")), this.toggles.click(this.toggleEvent);
            },
            toggleEvent: function () {
                var e = R("#" + R(this).attr("aria-controls"));
                e.length && (e.is(":visible") ? screenMeta.close(e, R(this)) : screenMeta.open(e, R(this)));
            },
            open: function (e, t) {
                R("#screen-meta-links").find(".screen-meta-toggle").not(t.parent()).css("visibility", "hidden"),
                    e.parent().show(),
                    e.slideDown("fast", function () {
                        e.focus(), t.addClass("screen-meta-active").attr("aria-expanded", !0);
                    }),
                    L.trigger("screen:options:open");
            },
            close: function (e, t) {
                e.slideUp("fast", function () {
                    t.removeClass("screen-meta-active").attr("aria-expanded", !1), R(".screen-meta-toggle").css("visibility", ""), e.parent().hide();
                }),
                    L.trigger("screen:options:close");
            },
        }),
        R(".contextual-help-tabs").delegate("a", "click", function (e) {
            var t,
                n = R(this);
            if ((e.preventDefault(), n.is(".active a"))) return !1;
            R(".contextual-help-tabs .active").removeClass("active"), n.parent("li").addClass("active"), (t = R(n.attr("href"))), R(".help-tab-content").not(t).removeClass("active").hide(), t.addClass("active").show();
        });
    var a = !1,
        r = R("#permalink_structure"),
        t = R(".permalink-structure input:radio"),
        c = R("#custom_selection"),
        n = R(".form-table.permalink-structure .available-structure-tags button");
    function l(e) {
        -1 !== r.val().indexOf(e.text().trim())
            ? (e.attr("data-label", e.attr("aria-label")), e.attr("aria-label", e.attr("data-used")), e.attr("aria-pressed", !0), e.addClass("active"))
            : e.attr("data-label") && (e.attr("aria-label", e.attr("data-label")), e.attr("aria-pressed", !1), e.removeClass("active"));
    }
    t.on("change", function () {
        "custom" !== this.value &&
            (r.val(this.value),
            n.each(function () {
                l(R(this));
            }));
    }),
        r.on("click input", function () {
            c.prop("checked", !0);
        }),
        r.on("focus", function (e) {
            (a = !0), R(this).off(e);
        }),
        n.each(function () {
            l(R(this));
        }),
        r.on("change", function () {
            n.each(function () {
                l(R(this));
            });
        }),
        n.on("click", function () {
            var e,
                t = r.val(),
                n = r[0].selectionStart,
                i = r[0].selectionEnd,
                o = R(this).text().trim(),
                s = R(this).attr("data-added");
            if (-1 !== t.indexOf(o)) return (t = t.replace(o + "/", "")), r.val("/" === t ? "" : t), R("#custom_selection_updated").text(s), void l(R(this));
            a || 0 !== n || 0 !== i || (n = i = t.length),
                c.prop("checked", !0),
                "/" !== t.substr(0, n).substr(-1) && (o = "/" + o),
                "/" !== t.substr(i, 1) && (o += "/"),
                r.val(t.substr(0, n) + o + t.substr(i)),
                R("#custom_selection_updated").text(s),
                l(R(this)),
                a && r[0].setSelectionRange && ((e = (t.substr(0, n) + o).length), r[0].setSelectionRange(e, e), r.focus());
        }),
        L.ready(function () {
            var n,
                i,
                o,
                s,
                e,
                t,
                a,
                r,
                c = !1,
                l = R("input.current-page"),
                d = l.val(),
                u = /iPhone|iPad|iPod/.test(navigator.userAgent),
                p = -1 !== navigator.userAgent.indexOf("Android"),
                f = R(document.documentElement).hasClass("ie8"),
                h = R("#adminmenuwrap"),
                m = R("#wpwrap"),
                v = R("#adminmenu"),
                b = R("#wp-responsive-overlay"),
                w = R("#wp-toolbar"),
                g = w.find('a[aria-haspopup="true"]'),
                k = R(".meta-box-sortables"),
                C = !1,
                x = R("#wpadminbar"),
                y = 0,
                S = !1,
                M = !1,
                E = 0,
                D = !1,
                T = { window: F.height(), wpwrap: m.height(), adminbar: x.height(), menu: h.height() },
                A = R(".wp-header-end");
            function j() {
                var e = R("a.wp-has-current-submenu");
                "folded" === r ? e.attr("aria-haspopup", "true") : e.attr("aria-haspopup", "false");
            }
            function _() {
                R(".notice.is-dismissible").each(function () {
                    var t = R(this),
                        e = R('<button type="button" class="notice-dismiss"><span class="screen-reader-text"></span></button>'),
                        n = commonL10n.dismiss || "";
                    e.find(".screen-reader-text").text(n),
                        e.on("click.wp-dismiss-notice", function (e) {
                            e.preventDefault(),
                                t.fadeTo(100, 0, function () {
                                    t.slideUp(100, function () {
                                        t.remove();
                                    });
                                });
                        }),
                        t.append(e);
                });
            }
            function I(e) {
                var t = F.scrollTop(),
                    n = !e || "scroll" !== e.type;
                if (!(u || f || v.data("wp-responsive")))
                    if (T.menu + T.adminbar < T.window || T.menu + T.adminbar + 20 > T.wpwrap) U();
                    else {
                        if (((D = !0), T.menu + T.adminbar > T.window)) {
                            if (t < 0) return void (S || ((M = !(S = !0)), h.css({ position: "fixed", top: "", bottom: "" })));
                            if (t + T.window > L.height() - 1) return void (M || ((S = !(M = !0)), h.css({ position: "fixed", top: "", bottom: 0 })));
                            y < t
                                ? S
                                    ? ((S = !1), (E = h.offset().top - T.adminbar - (t - y)) + T.menu + T.adminbar < t + T.window && (E = t + T.window - T.menu - T.adminbar), h.css({ position: "absolute", top: E, bottom: "" }))
                                    : !M && h.offset().top + T.menu < t + T.window && ((M = !0), h.css({ position: "fixed", top: "", bottom: 0 }))
                                : t < y
                                ? M
                                    ? ((M = !1), (E = h.offset().top - T.adminbar + (y - t)) + T.menu > t + T.window && (E = t), h.css({ position: "absolute", top: E, bottom: "" }))
                                    : !S && h.offset().top >= t + T.adminbar && ((S = !0), h.css({ position: "fixed", top: "", bottom: "" }))
                                : n && ((S = M = !1), 0 < (E = t + T.window - T.menu - T.adminbar - 1) ? h.css({ position: "absolute", top: E, bottom: "" }) : U());
                        }
                        y = t;
                    }
            }
            function O() {
                T = { window: F.height(), wpwrap: m.height(), adminbar: x.height(), menu: h.height() };
            }
            function U() {
                !u && D && ((S = M = D = !1), h.css({ position: "", top: "", bottom: "" }));
            }
            function K() {
                O(), v.data("wp-responsive") ? (V.removeClass("sticky-menu"), U()) : T.menu + T.adminbar > T.window ? (I(), V.removeClass("sticky-menu")) : (V.addClass("sticky-menu"), U());
            }
            function z() {
                R(".aria-button-if-js").attr("role", "button");
            }
            function N() {
                var e = !1;
                return W.innerWidth && (e = Math.max(W.innerWidth, document.documentElement.clientWidth)), e;
            }
            function P() {
                var e = N() || 961;
                (r = e <= 782 ? "responsive" : V.hasClass("folded") || (V.hasClass("auto-fold") && e <= 960 && 782 < e) ? "folded" : "open"), L.trigger("wp-menu-state-set", { state: r });
            }
            v.on("click.wp-submenu-head", ".wp-submenu-head", function (e) {
                R(e.target).parent().siblings("a").get(0).click();
            }),
                R("#collapse-button").on("click.collapse-menu", function () {
                    var e = N() || 961;
                    R("#adminmenu div.wp-submenu").css("margin-top", ""),
                        (r =
                            e < 960
                                ? V.hasClass("auto-fold")
                                    ? (V.removeClass("auto-fold").removeClass("folded"), setUserSetting("unfold", 1), setUserSetting("mfold", "o"), "open")
                                    : (V.addClass("auto-fold"), setUserSetting("unfold", 0), "folded")
                                : V.hasClass("folded")
                                ? (V.removeClass("folded"), setUserSetting("mfold", "o"), "open")
                                : (V.addClass("folded"), setUserSetting("mfold", "f"), "folded")),
                        L.trigger("wp-collapse-menu", { state: r });
                }),
                L.on("wp-menu-state-set wp-collapse-menu wp-responsive-activate wp-responsive-deactivate", j),
                ("ontouchstart" in W || /IEMobile\/[1-9]/.test(navigator.userAgent)) &&
                    ((e = u ? "touchstart" : "click"),
                    V.on(e + ".wp-mobile-hover", function (e) {
                        v.data("wp-responsive") || R(e.target).closest("#adminmenu").length || v.find("li.opensub").removeClass("opensub");
                    }),
                    v.find("a.wp-has-submenu").on(e + ".wp-mobile-hover", function (e) {
                        var t = R(this).parent();
                        v.data("wp-responsive") || t.hasClass("opensub") || (t.hasClass("wp-menu-open") && !(t.width() < 40)) || (e.preventDefault(), v.find("li.opensub").removeClass("opensub"), t.addClass("opensub"));
                    })),
                u ||
                    p ||
                    (v.find("li.wp-has-submenu").hoverIntent({
                        over: function () {
                            var e = R(this),
                                t = e.find(".wp-submenu"),
                                n = parseInt(t.css("top"), 10);
                            isNaN(n) || -5 < n || v.data("wp-responsive") || (v.find("li.opensub").removeClass("opensub"), e.addClass("opensub"));
                        },
                        out: function () {
                            v.data("wp-responsive") || R(this).removeClass("opensub").find(".wp-submenu").css("margin-top", "");
                        },
                        timeout: 200,
                        sensitivity: 7,
                        interval: 90,
                    }),
                    v
                        .on("focus.adminmenu", ".wp-submenu a", function (e) {
                            v.data("wp-responsive") || R(e.target).closest("li.menu-top").addClass("opensub");
                        })
                        .on("blur.adminmenu", ".wp-submenu a", function (e) {
                            v.data("wp-responsive") || R(e.target).closest("li.menu-top").removeClass("opensub");
                        })
                        .find("li.wp-has-submenu.wp-not-current-submenu")
                        .on("focusin.adminmenu", function () {
                            R(this);
                        })),
                A.length || (A = R(".wrap h1, .wrap h2").first()),
                R("div.updated, div.error, div.notice").not(".inline, .below-h2").insertAfter(A),
                L.on("wp-updates-notice-added wp-plugin-install-error wp-plugin-update-error wp-plugin-delete-error wp-theme-install-error wp-theme-delete-error", _),
                screenMeta.init(),
                V.on("click", "tbody > tr > .check-column :checkbox", function (e) {
                    if ("undefined" == e.shiftKey) return !0;
                    if (e.shiftKey) {
                        if (!c) return !0;
                        (n = R(c).closest("form").find(":checkbox").filter(":visible:enabled")),
                            (i = n.index(c)),
                            (o = n.index(this)),
                            (s = R(this).prop("checked")),
                            0 < i &&
                                0 < o &&
                                i != o &&
                                (i < o ? n.slice(i, o) : n.slice(o, i)).prop("checked", function () {
                                    return !!R(this).closest("tr").is(":visible") && s;
                                });
                    }
                    var t = R((c = this))
                        .closest("tbody")
                        .find(":checkbox")
                        .filter(":visible:enabled")
                        .not(":checked");
                    return (
                        R(this)
                            .closest("table")
                            .children("thead, tfoot")
                            .find(":checkbox")
                            .prop("checked", function () {
                                return 0 === t.length;
                            }),
                        !0
                    );
                }),
                V.on("click.wp-toggle-checkboxes", "thead .check-column :checkbox, tfoot .check-column :checkbox", function (e) {
                    var t = R(this),
                        n = t.closest("table"),
                        i = t.prop("checked"),
                        o = e.shiftKey || t.data("wp-toggle");
                    n
                        .children("tbody")
                        .filter(":visible")
                        .children()
                        .children(".check-column")
                        .find(":checkbox")
                        .prop("checked", function () {
                            return !R(this).is(":hidden,:disabled") && (o ? !R(this).prop("checked") : !!i);
                        }),
                        n
                            .children("thead,  tfoot")
                            .filter(":visible")
                            .children()
                            .children(".check-column")
                            .find(":checkbox")
                            .prop("checked", function () {
                                return !o && !!i;
                            });
                }),
                R("#wpbody-content").on(
                    {
                        focusin: function () {
                            clearTimeout(t), (a = R(this).find(".row-actions")), R(".row-actions").not(this).removeClass("visible"), a.addClass("visible");
                        },
                        focusout: function () {
                            t = setTimeout(function () {
                                a.removeClass("visible");
                            }, 30);
                        },
                    },
                    ".has-row-actions"
                ),
                R("tbody").on("click", ".toggle-row", function () {
                    R(this).closest("tr").toggleClass("is-expanded");
                }),
                R("#default-password-nag-no").click(function () {
                    return setUserSetting("default_password_nag", "hide"), R("div.default-password-nag").hide(), !1;
                }),
                R("#newcontent").bind("keydown.wpevent_InsertTab", function (e) {
                    var t,
                        n,
                        i,
                        o,
                        s = e.target;
                    if (27 == e.keyCode) return e.preventDefault(), void R(s).data("tab-out", !0);
                    9 != e.keyCode ||
                        e.ctrlKey ||
                        e.altKey ||
                        e.shiftKey ||
                        (R(s).data("tab-out")
                            ? R(s).data("tab-out", !1)
                            : ((t = s.selectionStart),
                              (n = s.selectionEnd),
                              (i = s.value),
                              document.selection
                                  ? (s.focus(), (document.selection.createRange().text = "\t"))
                                  : 0 <= t && ((o = this.scrollTop), (s.value = i.substring(0, t).concat("\t", i.substring(n))), (s.selectionStart = s.selectionEnd = t + 1), (this.scrollTop = o)),
                              e.stopPropagation && e.stopPropagation(),
                              e.preventDefault && e.preventDefault()));
                }),
                l.length &&
                    l.closest("form").submit(function () {
                        -1 == R('select[name="action"]').val() && -1 == R('select[name="action2"]').val() && l.val() == d && l.val("1");
                    }),
                R('.search-box input[type="search"], .search-box input[type="submit"]').mousedown(function () {
                    R('select[name^="action"]').val("-1");
                }),
                R("#contextual-help-link, #show-settings-link").on("focus.scroll-into-view", function (e) {
                    e.target.scrollIntoView && e.target.scrollIntoView(!1);
                }),
                (function () {
                    var e,
                        t,
                        n = R("form.wp-upload-form");
                    function i() {
                        e.prop(
                            "disabled",
                            "" ===
                                t
                                    .map(function () {
                                        return R(this).val();
                                    })
                                    .get()
                                    .join("")
                        );
                    }
                    n.length && ((e = n.find('input[type="submit"]')), (t = n.find('input[type="file"]')), i(), t.on("change", i));
                })(),
                u ||
                    (F.on("scroll.pin-menu", I),
                    L.on("tinymce-editor-init.pin-menu", function (e, t) {
                        t.on("wp-autoresize", O);
                    })),
                (W.wpResponsive = {
                    init: function () {
                        var e = this;
                        L.on("wp-responsive-activate.wp-responsive", function () {
                            e.activate();
                        }).on("wp-responsive-deactivate.wp-responsive", function () {
                            e.deactivate();
                        }),
                            R("#wp-admin-bar-menu-toggle a").attr("aria-expanded", "false"),
                            R("#wp-admin-bar-menu-toggle").on("click.wp-responsive", function (e) {
                                e.preventDefault(),
                                    x.find(".hover").removeClass("hover"),
                                    m.toggleClass("wp-responsive-open"),
                                    m.hasClass("wp-responsive-open") ? (R(this).find("a").attr("aria-expanded", "true"), R("#adminmenu a:first").focus()) : R(this).find("a").attr("aria-expanded", "false");
                            }),
                            v.on("click.wp-responsive", "li.wp-has-submenu > a", function (e) {
                                v.data("wp-responsive") && (R(this).parent("li").toggleClass("selected"), e.preventDefault());
                            }),
                            e.trigger(),
                            L.on("wp-window-resized.wp-responsive", R.proxy(this.trigger, this)),
                            F.on("load.wp-responsive", function () {
                                (-1 < navigator.userAgent.indexOf("AppleWebKit/") ? F.width() : W.innerWidth) <= 782 && e.disableSortables();
                            });
                    },
                    activate: function () {
                        K(), V.hasClass("auto-fold") || V.addClass("auto-fold"), v.data("wp-responsive", 1), this.disableSortables();
                    },
                    deactivate: function () {
                        K(), v.removeData("wp-responsive"), this.enableSortables();
                    },
                    trigger: function () {
                        var e = N();
                        e && (e <= 782 ? C || (L.trigger("wp-responsive-activate"), (C = !0)) : C && (L.trigger("wp-responsive-deactivate"), (C = !1)), e <= 480 ? this.enableOverlay() : this.disableOverlay());
                    },
                    enableOverlay: function () {
                        0 === b.length &&
                            (b = R('<div id="wp-responsive-overlay"></div>')
                                .insertAfter("#wpcontent")
                                .hide()
                                .on("click.wp-responsive", function () {
                                    w.find(".menupop.hover").removeClass("hover"), R(this).hide();
                                })),
                            g.on("click.wp-responsive", function () {
                                b.show();
                            });
                    },
                    disableOverlay: function () {
                        g.off("click.wp-responsive"), b.hide();
                    },
                    disableSortables: function () {
                        if (k.length)
                            try {
                                k.sortable("disable");
                            } catch (e) {}
                    },
                    enableSortables: function () {
                        if (k.length)
                            try {
                                k.sortable("enable");
                            } catch (e) {}
                    },
                }),
                R(document).ajaxComplete(function () {
                    z();
                }),
                L.on("wp-window-resized.set-menu-state", P),
                L.on("wp-menu-state-set wp-collapse-menu", function (e, t) {
                    var n = R("#collapse-button"),
                        i = "true",
                        o = commonL10n.collapseMenu;
                    "folded" === t.state && ((i = "false"), (o = commonL10n.expandMenu)), n.attr({ "aria-expanded": i, "aria-label": o });
                }),
                W.wpResponsive.init(),
                K(),
                P(),
                j(),
                _(),
                z(),
                L.on("wp-pin-menu wp-window-resized.pin-menu postboxes-columnchange.pin-menu postbox-toggled.pin-menu wp-collapse-menu.pin-menu wp-scroll-start.pin-menu", K),
                R(".wp-initial-focus").focus(),
                V.on("click", ".js-update-details-toggle", function () {
                    var e = R(this).closest(".js-update-details"),
                        t = R("#" + e.data("update-details"));
                    t.hasClass("update-details-moved") || t.insertAfter(e).addClass("update-details-moved"), t.toggle(), R(this).attr("aria-expanded", t.is(":visible"));
                });
        }),
        (function () {
            var e;
            function t() {
                L.trigger("wp-window-resized");
            }
            F.on("resize.wp-fire-once", function () {
                W.clearTimeout(e), (e = W.setTimeout(t, 200));
            });
        })(),
        (function () {
            if ("-ms-user-select" in document.documentElement.style && navigator.userAgent.match(/IEMobile\/10\.0/)) {
                var e = document.createElement("style");
                e.appendChild(document.createTextNode("@-ms-viewport{width:auto!important}")), document.getElementsByTagName("head")[0].appendChild(e);
            }
        })();
})(jQuery, window);
//# sourceMappingURL=common.min.js.map
